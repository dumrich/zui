use std::io;
use std::thread;
use std::time::Duration;
use zui_core::color::{self, Color};
use zui_core::style;
use zui_core::term::Terminal;

fn main() {
    // Create an example terminal
    let mut output = io::stdout();
    let mut my_term = Terminal::new(&mut output).unwrap();

    // Test setting the cursor and changing the color
    my_term.switch_screen().unwrap();

    let (x, y) = my_term.get_size(); // TODO: Fix this
    my_term.set_cursor_to(1, y).unwrap();
    my_term.print("asdf").unwrap();
    my_term.show_cursor().unwrap();
    my_term.switch_main().unwrap();

    // Test hiding the cursor
    thread::sleep(Duration::from_secs(2));
    my_term.hide_cursor().unwrap();

    // Test showing the cursor
    thread::sleep(Duration::from_secs(2));
    my_term.show_cursor().unwrap();

    // Test changing the cursor shape
    thread::sleep(Duration::from_secs(2));
    my_term.blinking_block().unwrap();

    // Test changing the cursor shape from another shape
    thread::sleep(Duration::from_secs(2));
    my_term.blinking_underline().unwrap();

    thread::sleep(Duration::from_secs(2));
    my_term.reset_cursor().unwrap();
}
